'use strict'
//Importar Librerias
const fs = require('fs');
//Leer Archivo Json
let data = fs.readFileSync('tasas.json');
let tasas = JSON.parse(data).RECORDS;
//Datos para calcular el interes
let vencimiento = new Date('2000-2-20');
let fechaPago = new Date('2020-5-15');
let ValorImpuesto = 250000;

function CalcularInteres(Valor, FechaVencimiento, FechaPagoDeuda) {
    vencimiento = new Date(FechaVencimiento);
    fechaPago = new Date(FechaPagoDeuda);
    ValorImpuesto = Valor;
    //Obtener total dias de mora
    var diasMora = CalcularDias(vencimiento, fechaPago);
    //Iterar tasas y calcular el interes
    for (var i = 0; i < tasas.length; i++) {
        //Obtener fecha de la tasa[i]
        var fechaTasa = new Date(tasas[i].fecha);
        //Calcular dias mora
        if (i == 0) {
            if (fechaTasa < vencimiento) tasas[i].dias = 0;
            else if (fechaTasa > fechaPago) tasas[i].dias = CalcularDias(vencimiento, fechaPago);
            else tasas[i].dias = CalcularDias(vencimiento, fechaTasa);
        }
        else {
            if (fechaTasa < vencimiento) tasas[i].dias = 0;
            else if (fechaTasa > fechaPago) tasas[i].dias = CalcularDias(fechaPago, vencimiento);

            else tasas[i].dias = CalcularDias(vencimiento, fechaTasa) - tasas.map(tasa => tasa.dias).reduce((acc, tasa) => tasa + acc);
        }
        //Calcular valor en forma trimestral para años menores a 2017
        var residuo = parseInt(tasas[i].mes) % 3;
        if (residuo == 0 && parseInt(tasas[i].anio) < 2017) {
            var mes = parseInt(tasas[i].mes);
            //Filtrar tasas del trimeste
            var TasasTrimestre = tasas.filter(todo => todo.anio == tasas[i].anio && todo.mes <= mes && todo.mes >= (mes - 2));
            //Sumar Dias
            var SumaDias = parseInt(TasasTrimestre.map(sum => sum.dias).reduce((acc, sum) => sum + acc));
            //asignar valor y redondear a 1000
            tasas[i].valor = Math.round((ValorImpuesto * ((tasas[i].tasa / 1000) / 366) * SumaDias) / 100) * 1000;
        }
    }
    return tasas.filter(p => p.anio == 2008 && (p.mes % 3) == 0);
}


function CalcularDias(FechaInicial, FechaFinal) {
    var diasMilisegundos = 86400000;
    var DiferenciaMilisegundos = FechaFinal - vencimiento;
    var DiferenciaDias = DiferenciaMilisegundos / diasMilisegundos;
    return DiferenciaDias;
}

module.exports={
    CalcularInteres
}